//  memuat dan menginisialisasi SDK
window.fbAsyncInit = () => {
    FB.init({
      appId      : '533045870374311',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // menjalankan fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)
    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login

    FB.AppEvents.logPageView();
    
        FB.getLoginStatus(function(response){
            if (response.status === 'connected'){
                render(true);
            }
            else {
                render(false);
            }
        });
  };

  // Call init facebook. default dari facebook
  
(function (d,s,id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)){
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri

const render = (loginFlag) => {
	if (loginFlag){
    // Jika yang akan dirender adalah tampilan sudah login
      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.

		getUserData(user => {
      // Render tampilan profil, form input post, tombol post status
			$('#lab8').html(
				'<div class="coverandpic">'+
					'<img class="picture" src="' + user.picture.data.url + '" alt="profpic" width="150px" height="150px" style="display:block; margin: 0 auto"/>' +
					'<img class="cover" src="' + user.cover.source + '" alt="cover" width="100%" style="clip: rect(0px,50px,50px,50px);"/>' +
				'</div>' +
				'<div class="userandabout">' +
					'<h1 align="center" style="font-weight:bold; color:white;">' + user.name + ' <a onclick="facebookLogout()"><i class="fa fa-sign-out" aria-hidden="true"></i></a>'+ '</h1>' +      
              		'<p align="center" style="font-weight:bold; color:white; font-size:17px; word-wrap: break-word;">' + user.about + '</p>' +
              	'</div>' +
              	'<hr/>' +
              	'<div class="container" id="emailgen">'+ 
              		'<p align="center" style="font-weight:bold; font-size:17px;"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:' + user.email +'">'+ user.email +'</a>' +' | '+ '  <i class="fa fa-venus-mars" aria-hidden="true"></i> ' + user.gender + '</p>' + 
		        '</div>'+
		        '<br>'+
		        '<section name="status-input" id="status-input" align="center">'+
		        	'<div>'+
		        		'<textarea class="form-control" id="postInput" type="text" rows="4" class="post" placeholder="What&apos;s on your mind?" />' +
		        		'<br>'+
		        		'<button class="btn btn-primary" onclick="postStatus()">Post to Facebook</button>' +
		        	'</div>'+
		        '</section>'+
		        '<h2 style="font-weight: bold;">'+ user.name +'&apos;s Recent Posts</h4>'+
		        '<hr/>'
		        //'<button class="logout" onclick="facebookLogout()">Logout</button>'

			);

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook

			getUserFeed(feed => {
				feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
					if (value.message && value.story){
						$('#lab8').append(
							'<div class="card">' +
                  				'<div class="card-body">' + value.message + '</div>' +
                  				'<br>'+
                  				'<div class="card-body">' + value.story + '</div>' +
                  				'<br>'+
                			'</div>'
						);
					}
					else if (value.message){
						$('#lab8').append(
							'<div class="card">' +
								'<br>'+
                  				'<div class="card-body">' + value.message + '</div>' +
                  				'<br>'+
                			'</div>'
						);
					}
					else if (value.story){
						$('#lab8').append(
							'<div class="card">' +
								'<br>'+
                  				'<div class="card-body">' + value.story + '</div>' +
                  				'<br>'+
                			'</div>'
						);
					} else if (value.story) {
						$('#lab8').append(
						  '<div class="feed">' +
							'<h2>' + value.story + '</h2>' +
							'<button class= "btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\' )">Delete</button>' +
						  '</div>'
						);
		  
					}
				});
			});
		});
	}
	else {
		$('#lab8').html('<div id="login-btn">' +
            '<button class="btn btn-primary" onclick="facebookLogin()">Login with Facebook</button>' +
            '</div>');
	}
};

const facebookLogin = () => {
   // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.

	FB.login(function(response){
		console.log(response);
		FB.api('/me/', {fields: 'name,email,picture.width(150).height(10)'}, function(response){
			console.log('Info: '+response.name+' '+response.email);
		});
		render(true);
	}, {scope:'public_profile,user_posts,publish_actions,user_about_me,email', auth_type:'rerequest'});
};

const facebookLogout = () => {
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.

	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			FB.logout();
			console.log("Berhasil logout!");
			render(false);
		}
	})
};

// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?

const getUserData = (fun) => {
	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			FB.api('/me?fields=id,name,about,cover,email,picture,gender', 'GET', function(response){
				console.log(response);
				if (response && !response.error){
					//picture = response.picture.data.url;
					//name = response.name;
					//userID = response.authResponse.userID || response.authResponse.userId;
					fun(response);
				}
				else {
					alert("Error getting User Data!");
				}
			});
		}
	});
};

// Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
// yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
// tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback


const getUserFeed = (fun) => {
	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			FB.api('/me/posts/', 'get', function(response){
				console.log(response);
				if (response && !response.error){
					fun(response);
				}
				else {
					alert("Error getting Feed!");
				}
			});
		}
	});
};

// Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
// Melalui API Facebook dengan message yang diterima dari parameter.


const postFeed = (message) => {
	FB.api('/me/feed', 'post', {message:message});
	alert("Your post has been published!");
	window.location.reload();
};

const postStatus = () => {
	const message = $('#postInput').val();
	console.log(message);
	$('#postInput').val("");
	postFeed(message);
};

const deleteStatus = (id) => {
    var postId = id;
    FB.api(postId, 'delete', function(response){
      document.location.reload();
    });
  }

var picture, name, userID;
